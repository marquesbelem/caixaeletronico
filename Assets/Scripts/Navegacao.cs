﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Navegacao : MonoBehaviour
{
    [SerializeField]
    private GameObject painel_ativa;

    [SerializeField]
    private GameObject painel_desativa;

    public void Acao()
    {
        painel_ativa.SetActive(true);
        painel_desativa.SetActive(false);
    }
}
